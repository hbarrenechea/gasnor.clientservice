﻿var video_out = document.getElementById("vid-box");
var vid_thumb = document.getElementById("vid-thumb");







angular.module('app', ['pubnub.angular.service'])
    .controller('ChatCtrl', ['$scope', 'Pubnub', function($scope, Pubnub) {
        $scope.messages = [];
        $scope.channel = 'messages-channel';

        $scope.messageContent = '';
        // Generating a random uuid between 1 and 100 using utility function from lodash library.
        $scope.uuid = Math.random().toString();
 
        // Please signup to PubNub to use your own keys: https://admin.pubnub.com/
        Pubnub.init({
            publish_key: 'pub-c-a1cd7ac1-585e-478e-925b-65d17ce62f7d',
            subscribe_key: 'sub-c-204f063e-c559-11e5-b764-02ee2ddab7fe',
            ssl: true,
            uuid: $scope.uuid
        });

        // Fetching a uniq random avatar from the robohash.org service.
        $scope.avatarUrl = function(uuid) {
            return '//robohash.org/' + uuid + '?set=set2&bgset=bg2&size=70x70';
        };

        // Send the messages over PubNub Network
        $scope.sendMessage = function() {
            // Don't send an empty message 
            if (!$scope.messageContent ||
                 $scope.messageContent === '') {
                return;
            }
            Pubnub.publish({
                channel: $scope.channel,
                message: {
                    content: $scope.messageContent,
                    sender_uuid: $scope.uuid,
                    date: new Date()
                },
                callback: function(m) {
                    console.log(m);
                }
            });
            // Reset the messageContent input
            $scope.messageContent = '';

        }

        // Subscribe to messages channel
        Pubnub.subscribe({
            channel: $scope.channel,
            triggerEvents: ['callback']
        });

        // Make it possible to scrollDown to the bottom of the messages container
        $scope.scrollDown = function(time) {
            var $elem = $('.collection');
            $('body').animate({
                scrollTop: $elem.height()
            }, time);
        };
        $scope.scrollDown(400);
        // Listenning to messages sent.
        $scope.$on(Pubnub.getMessageEventNameFor($scope.channel), function(ngEvent, m) {
            $scope.$apply(function() {
                $scope.messages.push(m)
            });
            $scope.scrollDown(400);
        });
    }]);

/*
 .controller('ChatCtrl', function ($scope, Pubnub) {
     $scope.channel = 'messages-channel';
     // Generating a random uuid between 1 and 100 using an utility function from the lodash library.         
     $scope.uuid = _.random(100).toString();
     Pubnub.init({
         publish_key: 'pub-c-cbfc33cd-ec13-4320-ac57-83705de7b3ab',
         subscribe_key: 'sub-c-eb12741e-9ac4-11e6-a681-02ee2ddab7fe',
         uuid: $scope.uuid
     });
     $scope.sendMessage = function () {
         // Don't send an empty message 

         if (!$scope.messageContent || $scope.messageContent === '') {
             return;
         }
         Pubnub.publish({
             channel: $scope.channel,
             message: {
                 content: $scope.messageContent,
                 sender_uuid: $scope.uuid,
                 date: new Date()
             },
             callback: function (m) {
                 console.log(m);
             }
         });
         // Reset the messageContent input
         $scope.messageContent = '';

     };

     $scope.messages = [];
     // Subscribing to the ‘messages-channel’ and trigering the message callback
     Pubnub.subscribe({
         channel: $scope.channel,
         triggerEvents: ['callback']
     });

     $scope.$on(Pubnub.getMessageEventNameFor($scope.channel), function (ngEvent, m) {
         $scope.$apply(function () {
             $scope.messages.push(m)
         });
     });

     // A function to display a nice uniq robot avatar 
     $scope.avatarUrl = function (uuid) {
         return 'http://robohash.org/' + uuid + '?set=set2&bgset=bg2&size=70x70';
     };

 });
 */




// Listening to the callbacks


var call = function ()
{
    
    var phone = window.phone = PHONE({
        number: Math.random() || "Anonymous", // listen on username line else Anonymous
        publish_key: 'pub-c-cbfc33cd-ec13-4320-ac57-83705de7b3ab',
        subscribe_key: 'sub-c-eb12741e-9ac4-11e6-a681-02ee2ddab7fe',
    });

    var ctrl = window.ctrl = CONTROLLER(phone);

    ctrl.ready(function () {
        phone.dial("contact");
        ctrl.addLocalStream(vid_thumb);
    });
    ctrl.receive(function (session) {
        session.connected(function (session) {
            video_out.appendChild(session.video);
            $('.clickToCall').hide();
            $('.calling').show();
        });
        session.ended(function (session) {
            ctrl.getVideoElement(session.number).remove();
        });
    });
   
    return false;
    
}

function end() {
    ctrl.hangup();
}

function mute() {
    var audio = ctrl.toggleAudio();
    if (!audio) $("#mute").html("Unmute");
    else $("#mute").html("Mute");
}

function pause() {
    var video = ctrl.toggleVideo();
    if (!video) $('#pause').html('Unpause');
    else $('#pause').html('Pause');
}
