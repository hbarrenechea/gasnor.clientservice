﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Gasnor.ClientService.Web.Startup))]
namespace Gasnor.ClientService.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
